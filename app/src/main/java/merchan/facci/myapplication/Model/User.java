package merchan.facci.myapplication.Model;

public class User {

    private String id;
    private String usuario;
    private String imageURL;
    private String status;
    private String search;

    public User(String id, String usuario, String imageURL, String status, String search) {
        this.id = id;
        this.usuario = usuario;
        this.imageURL = imageURL;
        this.status = status;
        this.search = search;
    }

    public User() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
